from fastapi import HTTPException, Depends, status
from fastapi.security import HTTPBasicCredentials, HTTPBasic
from passlib.context import CryptContext
from auth.jwt_handler import decode_jwt
from auth.jwt_bearer import JWTBearer
from database.database import user_collection
from models.user import User

security = HTTPBasic()
hash_helper = CryptContext(schemes=["bcrypt"])
token_listener = JWTBearer()


async def validate_login(credentials: HTTPBasicCredentials = Depends(security)):
    user = user_collection.find_one({"email": credentials.username})
    if user:
        password = hash_helper.verify(credentials.password, user['password'])
        if not password:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect email or password"
            )
        return True
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Incorrect email or password"
    )


async def get_current_user(token: str = Depends(token_listener)):
    user_email = (decode_jwt(token)['user_id'])
    user = await User.find_one(User.email == user_email)
    return user


class RoleChecker:
    def __init__(self, allowed_roles: list):
        self.allowed_roles = allowed_roles

    def __call__(self, user: User = Depends(get_current_user)):
        print(user)
        if user.role not in self.allowed_roles:
            raise HTTPException(status_code=403, detail="Operation not permitted")
