from beanie import Document
from typing import Union, Set, Optional
from fastapi.security import HTTPBasicCredentials
from pydantic import BaseModel, EmailStr, Field
from passlib.context import CryptContext

hash_helper = CryptContext(schemes=["bcrypt"])


class User(Document):
    name: str
    username: str
    email: EmailStr
    password: Union[str] = hash_helper.encrypt('3xt3m#')
    role: Union[str] = 'user'

    class Collection:
        name = "user"

    class Config:
        schema_extra = {
            "example": {
                "name": "Abdulazeez Abdulazeez Adeshina",
                "username": "abdul",
                "email": "abdul@youngest.dev",
                "password": "3xt3m#"
            }
        }


class UserSignIn(HTTPBasicCredentials):
    class Config:
        schema_extra = {
            "example": {
                "username": "abdul@youngest.dev",
                "password": "3xt3m#"
            }
        }


class UserData(BaseModel):
    username: str
    name: str
    email: EmailStr

    class Config:
        schema_extra = {
            "example": {
                "name": "Abdulazeez Abdulazeez Adeshina",
                "username": "abdul",
                "email": "abdul@youngest.dev",
            }
        }


class UserPassword(BaseModel):
    old_password: str
    new_password: str

    class Config:
        schema_extra = {
            "example": {
                "old_password": "3xt3m#",
                "new_password": "3xt3m#",
            }
        }
