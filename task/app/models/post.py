from pydantic import BaseModel
from beanie import Document
from typing import Optional, Any, Union


class Post(Document):
    id: str
    userId: str
    title: str
    body: str

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "id": 1,
                "userId": 1,
                "title": "Some title",
                "body": "Some body ",
            }
        }


class UpdatePostModel(Post):

    class Collection:
        name = "post"


class Response(BaseModel):
    status_code: int
    response_type: str
    description: str
    data: Optional[Any]

    class Config:
        schema_extra = {
            "example": {
                "status_code": 200,
                "response_type": "success",
                "description": "Operation successful",
                "data": "date"
            }
        }
