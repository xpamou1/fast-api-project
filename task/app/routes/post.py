from fastapi import APIRouter, Body, Request
from fastapi import BackgroundTasks, FastAPI, Depends
from database.database import *
from models.post import *
from tasks.sync import jsonplaceholder
from auth.user import get_current_user, RoleChecker
from models.user import User

router = APIRouter()
allow_resource = RoleChecker(["admin"])


@router.get("/", response_description="posts retrieved", response_model=Response)
async def get_posts(user: User = Depends(get_current_user)):
    posts = await retrieve_posts(user)
    return {
        "status_code": 200,
        "response_type": "success",
        "description": "Posts data retrieved successfully",
        "data": posts
    }


@router.get("/sync", response_description="posts retrieved", response_model=Response)
async def get_posts(background_tasks: BackgroundTasks):
    background_tasks.add_task(jsonplaceholder)
    posts = await retrieve_posts()
    return {
        "status_code": 200,
        "response_type": "success",
        "description": "Posts data retrieved successfully",
        "data": posts
    }


@router.get("/{id}", response_description="Post data retrieved", response_model=Response)
async def get_post_data(id: str, user: User = Depends(get_current_user)):
    post = await retrieve_post(id, user)
    if post:
        return {
            "status_code": 200,
            "response_type": "success",
            "description": "Post data retrieved successfully",
            "data": post
        }
    return {
        "status_code": 404,
        "response_type": "error",
        "description": "Post doesn't exist",
    }


@router.post("/", response_description="Post data added into the database", response_model=Response)
async def add_post_data(post: Post = Body(...), user: User = Depends(get_current_user)):
    post.userId = user.email
    new_post = await add_post(post, user)
    if new_post:
        return {
            "status_code": 200,
            "response_type": "success",
            "description": "Post created successfully",
            "data": new_post
        }
    return {
        "status_code": 409,
        "response_type": "error",
        "description": "Post already exist"
    }


@router.delete("/{id}", response_description="Post data deleted from the database",
               dependencies=[Depends(allow_resource)])
async def delete_post_data(id: str):
    delete_data = await delete_post(id)
    if delete_data:
        return {
            "status_code": 200,
            "response_type": "success",
            "description": "Post with ID: {} removed".format(id),
            "data": delete_data
        }
    return {
        "status_code": 404,
        "response_type": "error",
        "description": "Post with id {0} doesn't exist".format(id),
        "data": False
    }


@router.put("{id}", response_model=Response)
async def update_post(id: str, req: UpdatePostModel = Body(...), user: User = Depends(get_current_user)):
    updated_post = await update_post_data(id, req.dict(), user)
    if updated_post:
        return {
            "status_code": 200,
            "response_type": "success",
            "description": "Post with ID: {} updated".format(id),
            "data": updated_post
        }
    return {
        "status_code": 404,
        "response_type": "error",
        "description": "An error occurred. Post with ID: {} not found".format(id),
        "data": False
    }
