from fastapi import Body, APIRouter, HTTPException
from passlib.context import CryptContext
from passlib.pwd import genword
from fastapi import Depends

from auth.jwt_handler import sign_jwt
from database.database import add_user, change_password
from models.user import User, UserData, UserSignIn, UserPassword
from auth.user import get_current_user

router = APIRouter()

hash_helper = CryptContext(schemes=["bcrypt"])


@router.post("/login")
async def user_login(user_credentials: UserSignIn = Body(...)):
    user_exists = await User.find_one(User.email == user_credentials.username)
    if user_exists:
        password = hash_helper.verify(
            user_credentials.password, user_exists.password)
        if password:
            return sign_jwt(user_credentials.username)

        raise HTTPException(
            status_code=403,
            detail="Incorrect email or password"
        )

    raise HTTPException(
        status_code=403,
        detail="Incorrect email or password"
    )


@router.post("/new", response_model=UserData)
async def user_signup(user: User = Body(...)):
    user_exists = await User.find_one(User.email == user.email)
    if user_exists:
        raise HTTPException(
            status_code=409,
            detail="User with email supplied already exists"
        )

    user.password = hash_helper.encrypt(user.password)
    user.role = 'user'
    new_user = await add_user(user)
    return new_user


@router.post("/change")
async def user_change_login(user: UserPassword = Body(...), current_user: User = Depends(get_current_user)):
    password = hash_helper.verify(
        user.old_password, current_user.password)
    if password:
        new_user_password = await change_password(current_user, hash_helper.encrypt(user.new_password))
        return new_user_password
    raise HTTPException(
        status_code=403,
        detail="Incorrect password"
    )


@router.get("/me")
async def read_users_me(current_user: User = Depends(get_current_user)):
    return {"name": current_user.name, "username": current_user.username, "email": current_user.email}


@router.on_event("startup")
async def startup_event():
    password = genword(entropy='secure')
    name = 'admin'
    username = 'admin'
    email = 'admin@test.ru'
    if await add_user(User(name=name, username=username, email=email, password=hash_helper.encrypt(password), role='admin')):
        print(f'ADMIN PASSWORD {password}')
