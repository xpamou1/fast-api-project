from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient
from pydantic import BaseSettings
import os
from models.user import User
from models.post import Post


class Settings(BaseSettings):
    MONGODB_USERNAME: str = (os.environ["MONGO_INITDB_ROOT_USERNAME"])
    MONGODB_PASSWORD: str = (os.environ["MONGO_INITDB_ROOT_PASSWORD"])
    MONGODB_URL: str = (os.environ["MONGO_INITDB_URL"])
    MONGODB_DB: str = (os.environ["MONGO_DB_NAME"])
    JSONPLACEHOLDER_URL: str = (os.environ["JSONPLACEHOLDER_URL"])

    DATABASE_URL: str = f"mongodb://{MONGODB_USERNAME}:{MONGODB_PASSWORD}@{MONGODB_URL}/{MONGODB_DB}?authSource=admin"

    SECRET_KEY: str = (os.environ["SECRET_KEY"])
    ALGORITHM: str = "HS256"


async def initiate_database():
    client = AsyncIOMotorClient(Settings().DATABASE_URL)
    await init_beanie(database=client[Settings().MONGODB_DB],
                      document_models=[User, Post])
