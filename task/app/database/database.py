from typing import List, Union

from beanie import PydanticObjectId

from models.user import User, UserPassword
from models.post import Post

user_collection = User
post_collection = Post


async def add_user(new_user: User) -> User:
    if await user_collection.find_one(User.email == new_user.email):
        return False
    user = await new_user.create()
    return user


async def retrieve_posts(user: User) -> List[Post]:
    if user.role == 'admin':
        posts = await post_collection.all().to_list()
    else:
        posts = await post_collection.find(Post.userId == user.email).to_list()
    return posts


async def add_post(new_post: Post) -> Post:
    if await post_collection.get(new_post.id):
        return False
    post = await new_post.create()
    return post


async def retrieve_post(id: PydanticObjectId, user: User) -> Post:
    post = await post_collection.get(id)
    if post:
        if post.userId == user.email or user.role == "admin":
            return post


async def change_password(current_user: User, new_password: str):
    user = await user_collection.find_one(User.email == current_user.email)
    if user:
        await user.update({"$set": {"password": new_password}})
        return 'Password changed'


async def delete_post(id: PydanticObjectId) -> bool:
    post = await post_collection.get(id)
    if post:
        await post.delete()
        return True


async def update_post_data(id: PydanticObjectId, data: dict, user) -> Union[bool, Post]:
    des_body = {k: v for k, v in data.items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in des_body.items()
    }}
    post = await post_collection.get(id)
    if post:
        if post.userId == user.email or user.role == "admin":
            await post.update(update_query)
            return post
    return False
