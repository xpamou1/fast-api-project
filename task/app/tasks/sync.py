import requests
from config.config import Settings
from database.database import add_post
from database.database import add_user
from models.post import Post
from models.user import User


async def jsonplaceholder_user(user_id):
    url = f'{Settings().JSONPLACEHOLDER_URL}users/{user_id}'
    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    user = response.json()
    name = user['name']
    username = user['username']
    email = user['email']
    await add_user(User(name=name, username=username, email=email))
    return email


async def jsonplaceholder():
    url = f'{Settings().JSONPLACEHOLDER_URL}posts'
    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)

    if response.status_code == 200:
        for i in response.json():
            id = i['id']
            userId = i['userId']
            title = i['title']
            body = i['body']
            userId = await jsonplaceholder_user(userId)
            await add_post(Post(id=id, userId=userId, title=title, body=body))
    else:
        return f"jsonplaceholder request status {response.status_code}"
